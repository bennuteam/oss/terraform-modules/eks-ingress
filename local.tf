locals {
  ingress_args = list(
    "--configmap=$(POD_NAMESPACE)/nginx-configuration",
    "--tcp-services-configmap=$(POD_NAMESPACE)/tcp-services",
    "--udp-services-configmap=$(POD_NAMESPACE)/udp-services",
    "--publish-service=$(POD_NAMESPACE)/ingress-nginx",
    "--annotations-prefix=nginx.ingress.kubernetes.io",
  )

  ingress_labels = {
    "app.kubernetes.io/name"    = "ingress-nginx"
    "app.kubernetes.io/part-of" = "ingress-nginx"
  }
}
