# Original manifests:
# - https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml
# - https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/provider/aws/service-l4.yaml
# - https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/provider/aws/patch-configmap-l4.yaml

resource "kubernetes_namespace" "ingress_nginx" {
  metadata {
    name   = "ingress-nginx"
    labels = local.ingress_labels
  }
}

resource "kubernetes_config_map" "nginx_configuration" {
  metadata {
    name      = "nginx-configuration"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels

  }

  # data for AWS
  data = {
    "use-proxy-protocol" = "true"
  }
}

resource "kubernetes_config_map" "tcp_services" {
  metadata {
    name      = "tcp-services"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }
}

resource "kubernetes_config_map" "udp_services" {
  metadata {
    name      = "udp-services"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }
}

resource "kubernetes_service_account" "nginx_ingress_serviceaccount" {
  metadata {
    name      = "nginx-ingress-serviceaccount"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }

  automount_service_account_token = true
}

resource "kubernetes_cluster_role" "nginx_ingress_clusterrole" {
  metadata {
    name   = "nginx-ingress-clusterrole"
    labels = local.ingress_labels
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps", "endpoints", "nodes", "pods", "secrets"]
    verbs      = ["list", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["events"]
    verbs      = ["create", "patch"]
  }

  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["extensions", "networking.k8s.io"]
    resources  = ["ingresses/status"]
    verbs      = ["update"]
  }
}

resource "kubernetes_role" "nginx_ingress_role" {
  metadata {
    name      = "nginx-ingress-role"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps", "namespaces", "pods", "secrets"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps"]
    # Defaults to "<election-id>-<ingress-class>"
    # Here: "<ingress-controller-leader>-<nginx>"
    # This has to be adapted if you change either parameter
    # when launching the nginx-ingress-controller.
    resource_names = ["ingress-controller-leader-nginx"]
    verbs          = ["get", "update"]
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps"]
    verbs      = ["create"]
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints"]
    verbs      = ["get"]
  }
}

resource "kubernetes_role_binding" "nginx_ingress_role_nisa_binding" {
  metadata {
    name      = "nginx-ingress-role-nisa-binding"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.nginx_ingress_role.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.nginx_ingress_serviceaccount.metadata.0.name
    namespace = kubernetes_service_account.nginx_ingress_serviceaccount.metadata.0.namespace
  }
}

resource "kubernetes_cluster_role_binding" "nginx_ingress_clusterrole_nisa_binding" {
  metadata {
    name   = "nginx-ingress-clusterrole-nisa-binding"
    labels = local.ingress_labels
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.nginx_ingress_clusterrole.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.nginx_ingress_serviceaccount.metadata.0.name
    namespace = kubernetes_service_account.nginx_ingress_serviceaccount.metadata.0.namespace
  }
}

resource "kubernetes_deployment" "nginx_ingress_controller" {
  metadata {
    name      = "nginx-ingress-controller"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }

  spec {
    replicas = 1

    selector {
      match_labels = local.ingress_labels
    }

    template {
      metadata {
        labels = local.ingress_labels

        annotations = {
          "prometheus.io/port"   = "10254"
          "prometheus.io/scrape" = "true"
        }
      }

      spec {
        automount_service_account_token = true
        # wait up to five minutes for the drain of connections
        termination_grace_period_seconds = 300
        service_account_name             = kubernetes_service_account.nginx_ingress_serviceaccount.metadata.0.name
        node_selector = {
          "kubernetes.io/os" = "linux"
        }

        container {
          name    = "nginx-ingress-controller"
          image   = var.ingress_image
          command = ["/nginx-ingress-controller"]
          args    = local.ingress_args
          security_context {
            allow_privilege_escalation = true
            capabilities {
              drop = ["ALL"]
              add  = ["NET_BIND_SERVICE"]
            }
            # www-data -> 101
            run_as_user = "101"
          }

          env {
            name = "POD_NAME"
            value_from {
              field_ref {
                field_path = "metadata.name"
              }
            }
          }

          env {
            name = "POD_NAMESPACE"
            value_from {
              field_ref {
                field_path = "metadata.namespace"
              }
            }
          }

          port {
            name           = "http"
            container_port = 80
            protocol       = "TCP"
          }

          port {
            name           = "https"
            container_port = 443
            protocol       = "TCP"
          }

          liveness_probe {
            failure_threshold = 3
            http_get {
              path   = "/healthz"
              port   = 10254
              scheme = "HTTP"
            }
            initial_delay_seconds = 10
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 10
          }

          readiness_probe {
            failure_threshold = 3
            http_get {
              path   = "/healthz"
              port   = 10254
              scheme = "HTTP"
            }
            initial_delay_seconds = 10
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 10
          }

          lifecycle {
            pre_stop {
              exec {
                command = [
                  "/wait-shutdown",
                ]
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_limit_range" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
  }

  spec {
    limit {
      type = "Container"
      min = {
        memory = "90Mi"
        cpu    = "100m"
      }
    }
  }
}

resource "kubernetes_service" "ingress_nginx" {

  metadata {
    name      = "ingress-nginx"
    namespace = kubernetes_namespace.ingress_nginx.metadata.0.name
    labels    = local.ingress_labels
    annotations = {
      # Enable PROXY protocol
      "service.beta.kubernetes.io/aws-load-balancer-proxy-protocol" = "*"
      # Ensure the ELB idle timeout is less than nginx keep-alive timeout. By default,
      # NGINX keep-alive is set to 75s. If using WebSockets, the value will need to be
      # increased to '3600' to avoid any potential issues.
      "service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout" = "60"
    }
  }

  spec {
    type     = "LoadBalancer"
    selector = local.ingress_labels

    port {
      name        = "http"
      port        = 80
      protocol    = "TCP"
      target_port = "http"
    }

    port {
      name        = "https"
      port        = 443
      protocol    = "TCP"
      target_port = "https"
    }
  }
}
