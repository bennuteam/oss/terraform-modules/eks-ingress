variable "ingress_image" {
  type    = string
  default = "quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.30.0"
}
